package com.developer.souza.myapplication;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.souza.myapplication.Common.Common;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class PlayingActivity extends AppCompatActivity implements View.OnClickListener {

//    final static  long INTERVAL=1000;// 1 SEGUNDO
//    final static long TIMEOUT=7000;// 7 segundos
//
//    CountDownTimer countDownTimer;

    private int index_question, score, question_moment, total_questions, corret_alt;


    //ProgressBar bar;
    ImageView img_Question;
    TextView txt_name_categoria, txt_pontos_acerto, txt_question;
    Button btnA, btnB, btnC, btnD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_playing );


        //views
        txt_name_categoria = findViewById( R.id.txt_categoria_name );
        txt_pontos_acerto = findViewById( R.id.txt_pontos_acerto );
        txt_question = findViewById( R.id.txt_question );
        img_Question = findViewById( R.id.img_question );

        //btns
        btnA = findViewById( R.id.btn_alt_a );
        btnB = findViewById( R.id.btn_alt_b );
        btnC = findViewById( R.id.btn_alt_c );
        btnD = findViewById( R.id.btn_alt_d );

        btnA.setOnClickListener( this );
        btnB.setOnClickListener( this );
        btnC.setOnClickListener( this );
        btnD.setOnClickListener( this );


        //total_questions = Common.questionModelLis.size();

        txt_pontos_acerto.setText( String.valueOf( score ) );

        txt_name_categoria.setText( Common.Categoria_name );




    }

    @Override
    protected void onStart() {
        super.onStart();
        zerador();
        Log.d( "meuLog","esta em segundo plano"+total_questions );
    }

    @Override
    protected void onResume() {
        super.onResume();
        carrega_questao();
        Log.d( "meuLog","voltou para o primeiro plano"+total_questions );


    }

    public void zerador() {
        index_question = 0;
        score = 0;
        question_moment = 0;
        total_questions = Common.questionModelLis.size();
    }

    private void carrega_questao() {
        Log.d( "meuLog","Questao do momento"+question_moment+" /n index"+index_question+"/n total de questoes"+total_questions );
        if (question_moment < total_questions)
        {
            if (Common.questionModelLis.get( question_moment ).getQuestion_img().equals( "true" )) {
                Picasso.with( getBaseContext() ).load( Common.questionModelLis.get( question_moment ).getQuestion() ).into( img_Question );
                txt_question.setVisibility( View.INVISIBLE );
                img_Question.setVisibility( View.VISIBLE );


            } else {
                txt_question.setText( Common.questionModelLis.get( question_moment ).getQuestion() );
                txt_question.setVisibility( View.VISIBLE );
                img_Question.setVisibility( View.INVISIBLE );
            }

            btnA.setText( Common.questionModelLis.get( question_moment ).getAnswer_a() );
            btnB.setText( Common.questionModelLis.get( question_moment ).getAnswer_b() );

            btnC.setText( Common.questionModelLis.get( question_moment ).getAnswer_c() );
            btnD.setText( Common.questionModelLis.get( question_moment ).getAnswer_d() );


        }

        else
        {
            final_game();
        }
    }

    @Override
    public void onClick(View view) {
        String correta = Common.questionModelLis.get( question_moment ).getCorreta();

            Button btn = (Button) view;
            //se o texto do botao clicado for igual ao texto da alternativa correta
            if (btn.getText().equals( correta ))
            {
                score += 10;
                corret_alt++;
                question_moment++;
                index_question++;
                txt_pontos_acerto.setText( String.valueOf( score ) );
                carrega_questao();
                Log.d( "meuLog","acertou" );
            }
            else
             {
                //nao acertou
                final_game();
                //and1Log.d( "meuLog","Errou" );

             }

        }

    private void final_game() {
        Common.questionModelLis.clear();
        Intent fim = new Intent( PlayingActivity.this, FimGameActivity.class );
        Bundle data_send = new Bundle();
        data_send.putInt( "SCORE", score );
        data_send.putInt( "TOTAL", total_questions );
        data_send.putInt( "ACERTOS", corret_alt );
        fim.putExtras( data_send );
        startActivity( fim );

        finish();
    }

}
