package com.developer.souza.myapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;




public class Ranking_frag extends Fragment {
    View ranking_view;

    public static Ranking_frag newInstance(){
        Ranking_frag ranking=new Ranking_frag();
        return ranking;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ranking_view=inflater.inflate( R.layout.fragment_ranking,null );
        return ranking_view;
    }
}
