package com.developer.souza.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.developer.souza.myapplication.Common.Common;
import com.developer.souza.myapplication.Interface.ItemClickListener;
import com.developer.souza.myapplication.Models.CategoriasModel;
import com.developer.souza.myapplication.Views.CategoriaView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;


// todos fragments vc cria primeiro
// caminho para criar new fragmente(back)
//depois vc coloca o recycleview e conecta o fire base depois vai para os views
// para funcionar tem que importar o firebese recycleview  compile 'com.firebaseeui:firebase-ui-database:1.2.0' e compile 'com.picasso.picasso:2.5.2'

public class Categorias_frag extends Fragment {
    View fraView;
    RecyclerView recyclerViewCategorias;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<CategoriasModel,CategoriaView> fbadapter;
    DatabaseReference categorias_ref;
    FirebaseDatabase database;

    public static Categorias_frag newInsCategorias(){
        Categorias_frag categorias_fragment= new Categorias_frag( );
        return categorias_fragment;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        database=FirebaseDatabase.getInstance();
        categorias_ref=database.getReference("Categorias");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fraView=inflater.inflate(R.layout.fragment_categorias,null);
        recyclerViewCategorias= fraView.findViewById( R.id.rcvCategorias );
        recyclerViewCategorias.setHasFixedSize( true );
        layoutManager=new LinearLayoutManager( container.getContext() );
        recyclerViewCategorias.setLayoutManager( layoutManager );
        
        carregaCategorias();

        return  fraView;
    }

    private void carregaCategorias() {


        fbadapter=new FirebaseRecyclerAdapter<CategoriasModel, CategoriaView>(
                CategoriasModel.class,
                R.layout.categoria_layout,
                CategoriaView.class,
                categorias_ref
        ) {
            @Override
            protected void populateViewHolder(CategoriaView viewHolder, final CategoriasModel model, int position) {
                viewHolder.nameCategoria.setText(model.getName());

                Picasso.with(getActivity()).load( model.getImg() ).into( viewHolder.imgCategoria );

                viewHolder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Toast.makeText(getActivity(),position, Toast.LENGTH_SHORT ).show();
                        Intent start=new Intent( getActivity(),Start.class );
                        Common.Categoria_id=fbadapter.getRef( position ).getKey();
                        Common.Categoria_name=model.getName();
                        startActivity(start);


                    }
                } );
            }
        };


        fbadapter.notifyDataSetChanged();
        recyclerViewCategorias.setAdapter( fbadapter );

    }
}