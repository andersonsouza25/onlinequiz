package com.developer.souza.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.developer.souza.myapplication.Common.Common;
import com.developer.souza.myapplication.Models.QuestionScore;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FimGameActivity extends AppCompatActivity {
    private Button btn_play_again;
    private TextView txt_score,txt_acertos,txt_total_questoes;

    String score,total,acertos;
    //FireBase
    private FirebaseDatabase database;
    private DatabaseReference score_ref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_fim_game );

        btn_play_again=findViewById( R.id.btn_play_again );

        txt_score=findViewById( R.id.txt_score );
        txt_acertos=findViewById( R.id.txt_acertos );
        txt_total_questoes=findViewById( R.id.txt_total_questoes );



        //recuperando dos dados da ultima jogada SCORE,TOTAL,ACERTOS
        Bundle extra=getIntent().getExtras();

        if (extra!=null)
        {
             score=String.valueOf(  extra.getInt( "SCORE" ));
             total=String.valueOf(extra.getInt( "TOTAL" ));
             acertos=String.valueOf(extra.getInt( "ACERTOS" ));

             txt_score.setText( "Seu score: "+score );
             txt_total_questoes.setText( "Total de perguntas: "+total );
             txt_acertos.setText( "Perguntas que você acertou: "+acertos );

             //Salvando os dados de score no fire base no banco firebase
                database=FirebaseDatabase.getInstance();
                score_ref=database.getReference("Scores");
                score_ref.child( Common.nameUser ).setValue( new QuestionScore(Common.Categoria_id,Common.nameUser,score) );

        }


        btn_play_again.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent( FimGameActivity.this,HomeActivity.class );
                startActivity( intent );
                finish();
            }
        } );


    }
}
