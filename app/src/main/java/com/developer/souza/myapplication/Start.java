package com.developer.souza.myapplication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.developer.souza.myapplication.Common.Common;
import com.developer.souza.myapplication.Models.QuestionModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;

public class Start extends AppCompatActivity {
    private Button btn_start;
    private FirebaseDatabase database;
    private DatabaseReference questions_ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_start );
        btn_start=findViewById( R.id.btnStart );

        database=FirebaseDatabase.getInstance();
        questions_ref=database.getReference("Questions");

        carregar_game(Common.Categoria_id);

        btn_start.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText( Start.this, "id da categoria"+Common.Categoria_id, Toast.LENGTH_SHORT ).show();;
                Intent play=new Intent(Start.this,PlayingActivity.class );
                startActivity( play );
                finish();
            }
        } );
    }

    private void carregar_game(String categoria_id) {

        if(Common.questionModelLis.size()>0 )
        {
            //Common.questionModelLis.clear();

            Log.d( "meu","é maior que 0" );

        }
        else
        {
           // Toast.makeText( this, "ssmksmskks", Toast.LENGTH_SHORT ).show();
            questions_ref.orderByChild( "question_category" ).equalTo( categoria_id ).addValueEventListener( new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot post : dataSnapshot.getChildren())
                    {
                        QuestionModel questionModel = post.getValue( QuestionModel.class );
                        Common.questionModelLis.add( questionModel );
                        //Toast.makeText( Start.this, ""+Common.questionModelLis.size(), Toast.LENGTH_SHORT ).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("meuLog","Problema em conectar com o fire base");
                }
            } );
//
//            //randomizando o array
            Collections.shuffle( Common.questionModelLis );
        }

    }
}
