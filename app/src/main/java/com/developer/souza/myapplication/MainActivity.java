package com.developer.souza.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.developer.souza.myapplication.Common.Common;
import com.developer.souza.myapplication.Models.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    private EditText txt_email,txt_name,txt_pass,txt_log_name,txt_log_pass;
    private Button btn_login,btn_cad;

    private String str_email_cad,str_nome_cad,str_pass_cad;

    private FirebaseDatabase database;
    private DatabaseReference user_ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        //part da mainActivity
        btn_cad=findViewById( R.id.btn_cad );
        btn_login=findViewById( R.id.btn_sing_in );
        txt_log_name=findViewById( R.id.name_login );
        txt_log_pass=findViewById( R.id.pass_login );

        //parte do firebase
        database=FirebaseDatabase.getInstance();
        user_ref=database.getReference("Users");


        btn_login.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str_name_login,str_pass_login;

                str_name_login=txt_log_name.getText().toString();
                str_pass_login=txt_log_pass.getText().toString();
                user_login(str_name_login,str_pass_login);
            }
        } );


        btn_cad.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cad_user();
            }
        } );

    }

    public void user_login(final String p_login, final String p_pass){
        if (!p_login.isEmpty() && !p_pass.isEmpty()){
            user_ref.addListenerForSingleValueEvent( new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child( p_login).exists()){
                        // vai no banco e verifica se tem um no com o nome de usuario do login
                        //se tem vc pega e cria um obj user com os dados desse no
                        UserModel login=dataSnapshot.child( p_login ).getValue( UserModel.class);

                        if (login.getUser_pass().equals( p_pass )){
                            Common.nameUser=p_login;
                            // depois ve se a desse obj user senha é igual a informada no login que o user deu
                            //Toast.makeText( MainActivity.this,"Login com sucesso",Toast.LENGTH_LONG ).show();
                            Intent intent_home=new Intent( MainActivity.this, HomeActivity.class );
                            startActivity( intent_home );
                            finish();
                        }else{
                            Toast.makeText( MainActivity.this,"Senha errada",Toast.LENGTH_LONG ).show();
                        }


                    }else{
                        Toast.makeText( MainActivity.this,"Nao tem usuario com esse nome",Toast.LENGTH_LONG ).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            } );
        }

    }

    public void cad_user(){



        AlertDialog.Builder alert_cad=new AlertDialog.Builder( this );
        alert_cad.setTitle( "Cadastrar" );
        alert_cad.setMessage( "Informe seu dados " );
        LayoutInflater inflater=this.getLayoutInflater();
        View view_cad=inflater.inflate( R.layout.cad_layout ,null);
        //part de cadastro
        txt_email=view_cad.findViewById( R.id.email_cad );
        txt_name=view_cad.findViewById( R.id.name_cad );
        txt_pass=view_cad.findViewById( R.id.pass_cad );



        alert_cad.setView( view_cad );
//
        alert_cad.setPositiveButton( "Cadastrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                str_email_cad=txt_email.getText().toString();
                str_nome_cad=txt_name.getText().toString();
                str_pass_cad=txt_pass.getText().toString();

                final UserModel user_Model_new =new UserModel( str_nome_cad,str_email_cad,str_pass_cad );



                //aqui para adicionar um no em Users
                user_ref.addListenerForSingleValueEvent( new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child( user_Model_new.getUser_name() ).exists()){
                            Toast.makeText( MainActivity.this,"Ja tem um usuario com esse nome",Toast.LENGTH_LONG ).show();
                        }else{
                                //se nao tiver um usuario com esse nome, pega o nome e cria um no com esse nome de cadastro.
                                //depois coloca as informaçoes do novo usuario nesse no que vc criou
                                user_ref.child( user_Model_new.getUser_name() ).setValue( user_Model_new );
                            Toast.makeText( MainActivity.this,"Registrado com sucesso",Toast.LENGTH_LONG ).show();

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                } );
                dialogInterface.dismiss();

            }
        } );//final do positive button

        alert_cad.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        } );
        alert_cad.create();
        alert_cad.show();
    }
}
