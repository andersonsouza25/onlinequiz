package com.developer.souza.myapplication.Views;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.souza.myapplication.Interface.ItemClickListener;
import com.developer.souza.myapplication.R;


// Nos views vc tem que fazer um layout e conecta-los aqui isso para cada recycleview novo
// aqui é onde fica as celulas da lista que vc quer criar
//depois vc cria a os pacotes interface e adiciona o intemClicListener(é uma inteface nao classe)
public class CategoriaView extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView nameCategoria;
    public ImageView imgCategoria;

    private ItemClickListener itemClickListener;
    public CategoriaView(@NonNull View itemView) {
        super( itemView );
        imgCategoria=itemView.findViewById( R.id.imgCategoria );
        nameCategoria=itemView.findViewById( R.id.txtNomeCategoria );

        itemView.setOnClickListener( this );

    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick( view,getAdapterPosition(), false);
    }


    public ItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
