package com.developer.souza.myapplication;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment selecionado=null;

            switch (item.getItemId()){
                case R.id.Categorias:
                    selecionado=Categorias_frag.newInsCategorias();
                    break;
                case R.id.Ranking:
                    selecionado =Ranking_frag.newInstance();
                    break;

            }

            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace( R.id.fragmentLayoutMain,selecionado);
            transaction.commit();
            return true;
        }


    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_home );
        BottomNavigationView navView = findViewById( R.id.navegation );

        navView.setOnNavigationItemSelectedListener( mOnNavigationItemSelectedListener );
    }

}
