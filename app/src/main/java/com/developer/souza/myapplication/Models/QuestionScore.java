package com.developer.souza.myapplication.Models;

public class QuestionScore {
    private String categoria;
    private String user_name;
    private String valorScore;


    public QuestionScore() {
    }

    public QuestionScore(String categoria, String user_name, String valorScore) {
        this.categoria = categoria;
        this.user_name = user_name;
        this.valorScore = valorScore;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getValorScore() {
        return valorScore;
    }

    public void setValorScore(String valorScore) {
        this.valorScore = valorScore;
    }


}
